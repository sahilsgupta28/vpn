#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <netdb.h>
#include <unistd.h>

#define CHK_SSL(err) if ((err) < 1) { ERR_print_errors_fp(stderr); exit(2); }
#define CHK_ERR(err,s) if ((err)==-1) { perror(s); exit(1); }

#define PORT_DEFAULT 4433 

SSL* setupTLSServer(SSL_METHOD *meth, SSL_CTX* ctx)
{
    SSL_library_init();         /* load encryption & hash algorithms for SSL */   
    SSL_load_error_strings();   /* load the error strings for good error reporting */
    SSLeay_add_ssl_algorithms();
    
    meth = (SSL_METHOD *)TLSv1_2_method();
    ctx = SSL_CTX_new(meth);
    
    // Set up the server certificate and private key
    SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, NULL); //Dont ask for client certificate
    
    SSL_CTX_use_certificate_file(ctx, "./cert_server/server-cert.pem", SSL_FILETYPE_PEM);
    SSL_CTX_use_PrivateKey_file(ctx, "./cert_server/server-key.pem", SSL_FILETYPE_PEM);

    // Create a new SSL structure for a connection
    SSL *ssl = SSL_new (ctx);
    return ssl;
}

int setupTCPServer(short port)
{
    int fdSer;
    struct sockaddr_in sa_server;

    fdSer = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    CHK_ERR(fdSer, "socket");
    
    memset (&sa_server, '\0', sizeof(sa_server));
    sa_server.sin_family      = AF_INET;
    sa_server.sin_addr.s_addr = INADDR_ANY;
    sa_server.sin_port        = htons (port);
    
    int err = bind(fdSer, (struct sockaddr*)&sa_server, sizeof(sa_server));
    CHK_ERR(err, "bind");
    
    err = listen(fdSer, 5);
    CHK_ERR(err, "listen");
    
    return fdSer;
}

void processRequest(SSL* ssl, int sock)
{
    char buf[1024];
    
    int len = SSL_read (ssl, buf, sizeof(buf) - 1);
    buf[len] = '\0';
    printf("Received(%d): %s\n",len, buf);

    // Construct and send the HTML page
    char *html =
	    "HTTP/1.1 200 OK\r\n"
	    "Content-Type: text/html\r\n\r\n"
	    "<!DOCTYPE html><html>"
	    "<head><title>Hello World</title></head>"
	    "<style>body {background-color: black}"
	    "h1 {font-size:3cm; text-align: center; color: white;"
	    "text-shadow: 0 0 3mm yellow}</style></head>"
	    "<body><h1>Hello, world!</h1></body></html>";
	
    SSL_write(ssl, html, strlen(html));
    SSL_shutdown(ssl);
    SSL_free(ssl);
}

int main(int argc, char *argv[])
{
    short port = PORT_DEFAULT;      //Server Port
    size_t fdSer;                   //Server Socket FD
    struct sockaddr_in sa_client;   //Client Socket
    size_t client_len;              //Client Socket Length
    SSL_METHOD *meth;
    SSL_CTX* ctx;
    SSL *ssl;
    
    if (argc > 1)
        port = atoi(argv[1]);
    
    printf("Starting Server at port %d\n", port);
    
    // Setup TLS
    ssl = setupTLSServer(meth, ctx);
    
    //Setup Socket
    fdSer = setupTCPServer(port);

    while(1) {
        printf("Waiting for connections..\n");
        int fdCli = accept(fdSer, (struct sockaddr*)&sa_client, &client_len);
        if (fork() == 0) { // The child process : Per Client
            close (fdSer);
            printf("Client %s:%d connected\n", inet_ntoa(sa_client.sin_addr), ntohs(sa_client.sin_port));
            
            SSL_set_fd (ssl, fdCli);
            int err = SSL_accept (ssl); //Handshake
            CHK_SSL(err);
            printf ("SSL connection established!\n");

            processRequest(ssl, fdCli);
            
            close(fdCli);
            return 0;
        } else { // The parent process : Server
            close(fdCli);
        }
    } //while
}
