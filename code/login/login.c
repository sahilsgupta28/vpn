#include <stdio.h>
#include <string.h>
#include <shadow.h>
#include <crypt.h>

#include <stdlib.h>
#include <termios.h>

char* getPassword()
{
    struct termios oflags, nflags;
    char *password = NULL;
    size_t plen = 0;

    /* disabling echo */
    tcgetattr(fileno(stdin), &oflags);
    nflags = oflags;
    nflags.c_lflag &= ~ECHO;        // disable echo
    nflags.c_lflag |= ECHONL;       // echo new line

    if (tcsetattr(fileno(stdin), TCSANOW, &nflags) != 0) {
        perror("tcsetattr");
        return NULL;
    }

    printf("Password: ");
    int len = getline(&password, &plen, stdin);
    password[len - 1] = '\0';

    /* restore terminal */
    if (tcsetattr(fileno(stdin), TCSANOW, &oflags) != 0) {
        free (password);
        perror("tcsetattr");
        return NULL;
    }

    return password;
}

int login(char *user, char *passwd)
{
    struct spwd *pw;
    char *epasswd;
    
    pw = getspnam(user);
    if (pw == NULL) {
        printf("Failed to fetch password for user\n");
        return -1;
    }
    
    printf("Login name: %s\n", pw->sp_namp);
    printf("Passwd : %s\n", pw->sp_pwdp);
    
    epasswd = crypt(passwd, pw->sp_pwdp);
    if (strcmp(epasswd, pw->sp_pwdp)) {
        printf("Password Match Failed\n");
        return -1;
    }
    
    return 1;
}

void main(int argc, char** argv)
{
    char *p = getPassword();   
    printf("you typed (%s)\n", p);
    
    if (argc < 2) {
        printf("Please provide a user name\n");
        return;
    }
    int r = login(argv[1], p);
    r == 1 ? printf("Success\n") : printf("Failed\n");
    
    free(p);
}
