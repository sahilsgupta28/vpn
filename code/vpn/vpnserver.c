#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>         //atoi
#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <sys/ioctl.h>
#include <shadow.h>         //login: getspnam
#include <crypt.h>          //login: crypt

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <netdb.h>

// TUN
#define TUN_DEV_NAME "/dev/net/tun"

// TCP 
#define TCP_PORT_DEFAULT 4433 
#define BUFF_SIZE 2000

// SSL
#define CHK_SSL(err) if ((err) < 1) { ERR_print_errors_fp(stderr); exit(2); }
#define CHK_ERR(err,s) if ((err)==-1) { perror(s); exit(1); }

// VPN Header
enum vpn_type {VPN_DATA, VPN_CONTROL};
enum vpn_control_flag {VPN_SUCCESS, VPN_FAIL, VPN_EXIT};

struct VpnHeader {
    enum vpn_type type;
    enum vpn_control_flag flag;
    int length;
};

// Pipe file descriptors for reading and writing
#define PIPE_FD_RD  0
#define PIPE_FD_WR  1

// Client Info
#define MAX_CLIENT              254
#define CLIENT_HOSTID_START     5
#define DOMAIN_NETID            "192.168.53."

// Client Info Structure
struct ClientInfo {
    struct in_addr  ipClient;   // Ip Address of Client
    struct in_addr  ipNew;      // New Ip Address of Client
    int             pipefd[2];  // Pipe file descriptor for parent-child communication
};

// The IP header's structure
struct ipheader {
	unsigned char      iph_ihl:4, iph_ver:4;
	unsigned char      iph_tos;
	unsigned short int iph_len;
	
	unsigned short int iph_ident;
    unsigned short int iph_flag:3, iph_offset:13;
	
	unsigned char      iph_ttl;
	unsigned char      iph_protocol;
	unsigned short int iph_chksum;
	
	unsigned int       iph_sourceip;
	unsigned int       iph_destip;
};

// ---------------------- TUN --------------------------------

int createTunDevice()
{
   int tunfd;
   struct ifreq ifr;
   memset(&ifr, 0, sizeof(ifr));

   ifr.ifr_flags = IFF_TUN | IFF_NO_PI;  //IFF_NO_PI tells the kernel to not provide packet information

   tunfd = open(TUN_DEV_NAME, O_RDWR);
   ioctl(tunfd, TUNSETIFF, &ifr);   //Create device      

   return tunfd;
}


int configureInterface()
{
    //Call script to assign IP to tun interface and activate it
    
    if(fork() == 0) //Child Process
    {
        char *newargv[] = { "./configserver.sh", NULL };
        char *newenviron[] = { NULL };
        
        execve(newargv[0], newargv, newenviron);
        
        perror("execve");   /* execve() returns only on error */
        exit(EXIT_FAILURE);
    }
}

// ---------------------- TCP --------------------------------

int pem_passwd_cb(char *buf, int size, int rwflag, void *password)
{
    //printf("pem_passwd_cb called with %d %s\n", rwflag, (char *)password);

    strncpy(buf, (char *)(password), size);
    buf[size - 1] = '\0';
    
    return(strlen(buf));
}

SSL* setupTLSServer(SSL_METHOD *meth, SSL_CTX* ctx)
{

    // Step 0: OpenSSL library initialization 
    SSL_library_init();         /* load encryption & hash algorithms for SSL */   
    SSL_load_error_strings();   /* load the error strings for good error reporting */
    SSLeay_add_ssl_algorithms();
    
    // Step 1: SSL context initialization
    meth = (SSL_METHOD *)TLSv1_2_method();
    ctx = SSL_CTX_new(meth);
    
    // Step 2: Set up server certificate and private key
    SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, NULL); //Dont ask for client certificate

    // Setup server-key password
    char *server_key_passcode = "servergupta";
    SSL_CTX_set_default_passwd_cb_userdata(ctx, (void *)server_key_passcode);   // Supply password
    SSL_CTX_set_default_passwd_cb(ctx, pem_passwd_cb);                          // Supply callback 

    // file location    
    SSL_CTX_use_certificate_file(ctx, "./cert_server_vpn/server-cert.pem", SSL_FILETYPE_PEM);
    SSL_CTX_use_PrivateKey_file(ctx, "./cert_server_vpn/server-key.pem", SSL_FILETYPE_PEM);

    // Step 3: Create a new SSL structure for a connection
    SSL *ssl = SSL_new (ctx);
    
    return ssl;
}

int setupTCPServer(short port)
{
    int fdSer;
    struct sockaddr_in sa_server;

    fdSer = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    CHK_ERR(fdSer, "socket");
    
    memset (&sa_server, '\0', sizeof(sa_server));
    sa_server.sin_family      = AF_INET;
    sa_server.sin_addr.s_addr = INADDR_ANY;
    sa_server.sin_port        = htons (port);
    
    int err = bind(fdSer, (struct sockaddr*)&sa_server, sizeof(sa_server));
    CHK_ERR(err, "bind");
    
    err = listen(fdSer, 5);     //5 is max queue length for pending connections
    CHK_ERR(err, "listen");
    
    return fdSer;
}

void SLLTestConnection(SSL* ssl, int sock)
{
    char buf[512];
    int len;
    
    printf("\nTesting Connection Start\n");
    len = SSL_read (ssl, buf, sizeof(buf) - 1);
    buf[len] = '\0';
    printf("Received(%d):%s\n",len, buf);

    // Construct and send the HTML page
    char *html =
	    "HTTP/1.1 200 OK\r\n"
	    "Content-Type: text/html\r\n\r\n"
	    "<!DOCTYPE html><html>"
	    "<head><title>Hello World</title></head>"
	    "<body><h1>Hello, world!</h1></body></html>";
    len = SSL_write(ssl, html, strlen(html));
    printf("Sent(%d):%s\n",len, html);
    printf("\nTesting Connection End\n");
}

// ---------------------- Client Info --------------------------------

// Search client for a given IP
// Optimized by using integer comparison for network address
int searchClient(struct ClientInfo clients[], int cid, struct in_addr *socCli)
{
    for (int i = CLIENT_HOSTID_START; i <= cid; i++) {
        if (clients[i].ipClient.s_addr == socCli->s_addr)
            return i;
    }

    return -1;
}

// Search New Client Ip for a given Network IP
// Optimized by using HostId of New IP as Index into ClientInfo table
int getClientHostIdFromN(unsigned int *networkIp)
{
    int iClientHostId;
    char sClientIp[INET_ADDRSTRLEN];
    struct in_addr ipaddr;
    
    // Get IP in string format
    ipaddr.s_addr = *networkIp;
    inet_ntop(AF_INET, &ipaddr, sClientIp, INET_ADDRSTRLEN);
    
    //Extract Host Id
    iClientHostId = atoi(strrchr(sClientIp, '.') + 1);
    
    //printf("getClientHostIdFromN(%s) : %d\n", sClientIp, iClientHostId);

    return iClientHostId;
}

int newClient(struct ClientInfo clients[], int cid, struct in_addr *socCli)
{
    char sClientIp[INET_ADDRSTRLEN], sClientHostId[4];
    
    // Check if client was already connected before
    int cidret = searchClient(clients, cid, socCli);
    if (cidret != -1) {
        // If client found, create new pipe and return old cid
        pipe(clients[cidret].pipefd);
        //printf("Existing Client[%d] Pipe[%d:%d]\n", cidret, clients[cidret].pipefd[PIPE_FD_RD], clients[cidret].pipefd[PIPE_FD_WR]);
        return cidret;    
    }
    
    //Create new client info entry
    memset(&clients[cid], 0, sizeof(struct ClientInfo));
    
    //Save client IP
    clients[cid].ipClient.s_addr = socCli->s_addr;
    
    // Create New Client IP Address by using unassigned HostIp
    snprintf(sClientHostId, sizeof(sClientHostId), "%d", cid);
    strcpy(sClientIp, DOMAIN_NETID);
    strcat(sClientIp, sClientHostId);
    //clients[cid].ipNew.s_addr = inet_addr(sClientIp);   //Save network address in ipNew
    int ret = inet_pton(AF_INET, sClientIp, &clients[cid].ipNew.s_addr);
    if(ret != 1) {
        printf("inet_pton FAILED\n");
    }
    
    // Create Pipe for communication 
    // between parent process (server) and child process (per client)
    pipe(clients[cid].pipefd);
    //printf("New Client[%d] NewIP[%s] Pipe[%d:%d]\n", cid, sClientIp, clients[cid].pipefd[PIPE_FD_RD], clients[cid].pipefd[PIPE_FD_WR]);
    
    return cid;
}

void displayClientInfo(struct ClientInfo clients[], int cnt)
{
    char ipClient[INET_ADDRSTRLEN], ipNew[INET_ADDRSTRLEN];
    
    for (int i = CLIENT_HOSTID_START; i < cnt; i++) {
        //printf("Client[%d] IP[%x] NewIp[%x] Pipe[%d:%d]\n", i, clients[i].ipClient.s_addr, clients[i].ipNew.s_addr, clients[i].pipefd[PIPE_FD_RD], clients[i].pipefd[PIPE_FD_WR]);
        //printf("Client[%d] IP[%s] NewIp[%s] Pipe[%d:%d]\n", i, inet_ntoa(clients[i].ipClient), inet_ntoa(clients[i].ipNew), clients[i].pipefd[PIPE_FD_RD], clients[i].pipefd[PIPE_FD_WR]);
        inet_ntop(AF_INET, &clients[i].ipClient, ipClient, INET_ADDRSTRLEN);
        inet_ntop(AF_INET, &clients[i].ipNew, ipNew, INET_ADDRSTRLEN);
        printf("Client[%d] IP[%s] NewIp[%s] Pipe[%d:%d]\n", i, ipClient, ipNew, clients[i].pipefd[PIPE_FD_RD], clients[i].pipefd[PIPE_FD_WR]);
    }
}

int assignClientIp(SSL* ssl, struct ClientInfo clients[], const int cid)
{
    char ipNew[INET_ADDRSTRLEN];
    
    inet_ntop(AF_INET, &clients[cid].ipNew, ipNew, INET_ADDRSTRLEN);
    
    int len = SSL_write(ssl, ipNew, strlen(ipNew));
    //printf("Sent(%d):%s\n",len, ipNew);
    
    printf("Assigning IP (%s) to client\n", ipNew);
    
    return (len > 0 ? 0 : -1);
}

// ---------------------- Tunnel --------------------------------

int TunToPipe(int tunfd, struct ClientInfo clients[])
{
    int iClientHostId;
    int  bufflen;
    char buff[BUFF_SIZE];
    
    // Read data from TUN Interface
    bzero(buff, sizeof(buff));
    bufflen = read(tunfd, buff, sizeof(buff));

    // Read IP from packet
	struct ipheader *ip = (struct ipheader *)buff;

    // Only process IPv4 Packets
	if(ip->iph_ver != 4) {
	    return 0;
	}
	
	static int cnt;
	struct in_addr ipdst;           //todo: typecase directly
	ipdst.s_addr = ip->iph_destip; 
    //printf("%3d TUN(%d)[%s] : %c\n", cnt++, bufflen, inet_ntoa(ipdst), buff[0]);
    
    // Get client host id for this IP packet
    // This will determine which child process (client) should we send this to
    iClientHostId = getClientHostIdFromN(&ip->iph_destip);
    //todo check if iClientHostId is valid

    // Write to pipe 
    write(clients[iClientHostId].pipefd[PIPE_FD_WR], buff, bufflen);
    return 0;
}

int PipeToSocket(int pipeRdFd, SSL *ssl)
{
    int  bufflen;
    char buff[BUFF_SIZE];
    
    // Read data from pipe
    bzero(buff, sizeof(buff));
    bufflen = read(pipeRdFd, buff, sizeof(buff));

    static int cnt;
    //printf("%3d PIP(%d) : %c\n", cnt++, bufflen, buff[0]);
    
    // Write to socket 
    SSL_write(ssl, buff, bufflen);
    return 0;
}

int socketToTun(SSL *ssl, int tunfd)
{
    int  bufflen;
    char buff[BUFF_SIZE];
    
    //Check if data or control channel
    struct VpnHeader hdr;
    memset(&hdr, 0, sizeof(hdr));
    bufflen = SSL_read(ssl, &hdr, sizeof(hdr));
    if(bufflen == 0) {
        printf("Connection terminated by client\n");
        return 1;
    } else if (bufflen < 0) {
        int ret = SSL_get_error(ssl, bufflen);
        printf("SSL_read Error(%d): %s\n", ret, ERR_error_string(ret, NULL));
        return 1;
    }

    //printf("VPN Header Type(%d) Length(%d)\n", hdr.type, hdr.length);

    if(VPN_CONTROL == hdr.type && VPN_EXIT == hdr.flag) {
        printf("Connection closed by client\n");
        return 1;
    }

    // Read data from socket
    // Get encrypted packets from client on socket interface, 
    // SSL will give decrypted packet
    bzero(buff, sizeof(buff));
    bufflen = SSL_read(ssl, buff, sizeof(buff));    //Read hdr.length here
    if(bufflen == 0) {
        printf("Connection terminated by client\n");
        return 1;
    } else if (bufflen < 0) {
        int ret = SSL_get_error(ssl, bufflen);
        printf("SSL_read Error(%d): %s\n", ret, ERR_error_string(ret, NULL));
        return 1;
    }

    static int cnt;
    //printf("%3d SOC(%d) : %c\n", cnt++, bufflen, buff[0]);
    
    // Write to TUN Interface which will forward to destination
    write(tunfd, buff, bufflen);
    return 0;
}

int processCommand(struct ClientInfo clients[], int iClientHostId)
{
    char buff[64];

    fgets(buff, sizeof buff, stdin);

    // Check for stop command
    char exitStr[] = "exit";    
	if(strncmp(exitStr, buff, strlen(exitStr)) == 0)
	    return 0;
	
	// Other commands to check
	char clientsStr[] = "clients";    
	if(strncmp(clientsStr, buff, strlen(clientsStr)) == 0)
	    displayClientInfo(clients, iClientHostId);

	return -1; 
}

// ---------------------------- login ----------------------------

int login(char *user, char *passwd)
{
    struct spwd *pw;
    char *epasswd;
    
    pw = getspnam(user);
    if (pw == NULL) {
        printf("Failed to fetch password for user\n");
        return 2;
    }
    
    //printf("User name: %s\n", pw->sp_namp);
    //printf("Passwd : %s\n", pw->sp_pwdp);
    
    epasswd = crypt(passwd, pw->sp_pwdp);
    if (strcmp(epasswd, pw->sp_pwdp)) {
        printf("Password Match Failed\n");
        return 1;
    }
    
    //printf("Password Matched\n");
    return 0;
}

int AuthenticateUser(SSL *ssl)
{
    char username[64], *password;
    
    // Get username/password from client
    int len = SSL_read (ssl, username, sizeof(username));
    username[len] = '\0';
    //printf("Recieved (%d): %s\n", len, username);
    
    // Extract username and password
	password = strchr(username, '\\');
	int ulen = password - username;
	username[ulen] = '\0';
	password++;
	
	// Check credientials in shadow file
    //printf("Testing Cred: [%s]:[%s]\n", username, password);    
    int status = login(username, password);
    
    // Send result to server
    struct VpnHeader hdr;
    memset(&hdr, 0, sizeof hdr);
    hdr.type = VPN_CONTROL;
    hdr.flag = (status == 0 ? VPN_SUCCESS : VPN_FAIL);
    
    len = SSL_write(ssl, &hdr, sizeof(hdr));
    //printf("Sent(%d): Flag(%d)\n", len, hdr.flag);
    
    return status;
}

// --------------------------- UTIL -------------------------------------------

void getclientinfo(int *fd, struct sockaddr_in* sa_client, int client_len)
{
    printf("Client %s:%hu connected\n", inet_ntoa(sa_client->sin_addr), ntohs(sa_client->sin_port));

    char host[1024], service[20];
    getnameinfo((struct sockaddr *)sa_client, client_len, host, sizeof (host), service, sizeof (service), 0);
    printf("getnameinfo: %s:%s\n", host, service);// e.g. "www.example.com" "http"

    socklen_t len;
    struct sockaddr_storage addr;
    char ipstr[INET6_ADDRSTRLEN];
    len = sizeof(addr);
    getpeername(*fd, (struct sockaddr*)&addr, &len);
    struct sockaddr_in *sa = (struct sockaddr_in *)&addr;
    inet_ntop(AF_INET, &sa->sin_addr, ipstr, sizeof ipstr);
    printf("getpeername(%d): %s:%hu\n", *fd, ipstr, ntohs(sa->sin_port));
}

void getIPfromFd(int fd)
{
    socklen_t len;
    struct sockaddr_in addrin;
    char ipstr[INET6_ADDRSTRLEN];
    
    len = sizeof(addrin);
    getpeername(fd, (struct sockaddr*)&addrin, &len);
    
    inet_ntop(AF_INET, &addrin.sin_addr, ipstr, sizeof ipstr);
    printf("getpeername(%d): %s:%hu\n", fd, ipstr, ntohs(addrin.sin_port));
}

// ---------------------- MAIN --------------------------------

int main (int argc, char * argv[])
{
    int tunfd;                      // Tun Interface FD
    size_t fdSer;                   // Server Socket FD
    short port = TCP_PORT_DEFAULT;  // Server Port
    
    struct sockaddr_in sa_client;   // Client Socket
    size_t client_len;              // Client Socket Length
        
    struct ClientInfo client[MAX_CLIENT];       //Client Info
    int iClientHostId = CLIENT_HOSTID_START;
    
    memset(&client, 0, sizeof(struct ClientInfo) * MAX_CLIENT);
    
    // Accept commandline arg for port number
    if(argc > 1) port = atoi(argv[1]);

    tunfd  = createTunDevice();         // Setup TUN Interface
    configureInterface();               // Configure TUN Interface
    
    fdSer  = setupTCPServer(port);      // Setup TCP Server
            
    // printf("Starting Server at port %d\n", port);
    printf("Waiting for client connections...\n");
    
    while (1) {
        fd_set serverFdSet;

        FD_ZERO(&serverFdSet);
        FD_SET(fdSer, &serverFdSet);
        FD_SET(tunfd, &serverFdSet);
        FD_SET(fileno(stdin), &serverFdSet);

        //Wait untill data is availabe on an interface
        select(FD_SETSIZE, &serverFdSet, NULL, NULL, NULL);

        // Process data from tun interface when available
        if (FD_ISSET(tunfd,  &serverFdSet)) {
            if(TunToPipe(tunfd, client))
                break;      //Error
            else 
                continue;   //Success
        }
        
        // Process command on console when available
        if (FD_ISSET(fileno(stdin), &serverFdSet)) {
            if(processCommand(client, iClientHostId) == 0)   // Enter "exit" to stop
                break;
            continue; 
	    }
            
        // Process data from socket when available
        if (!FD_ISSET(fdSer, &serverFdSet)) {
            printf("Error: No Interface Selected\n");
            continue;        
        }
        
        // Accept new client connection
        client_len = sizeof(sa_client);
        int fdCli = accept(fdSer, (struct sockaddr*)&sa_client, &client_len);
        
        // Create new client info entry, create pipe
        int cid = newClient(client, iClientHostId, &sa_client.sin_addr);
        if(cid == iClientHostId) {  //If New Client Connection
            iClientHostId++;    // If this is new client connection, then only increment hostid
        } // Else this is old client reconnecting. 
        
        //displayClientInfo(client, iClientHostId);

        if (fork() == 0) { // The child process : Per Client
            close (fdSer);  //Close server listening socket
            close(client[cid].pipefd[PIPE_FD_WR]); //Close write end of pipe

            printf("Client %s:%hu connected\n", inet_ntoa(sa_client.sin_addr), ntohs(sa_client.sin_port));
            //getclientinfo(&fdCli, &sa_client, client_len);
            //getIPfromFd(fdCli);
            
            //Needs Access to : tun, fdCli, client, cid
            
            SSL_METHOD *meth;               // SSL
            SSL_CTX* ctx;
            SSL *ssl;
                
            ssl    = setupTLSServer(meth, ctx); // Setup TLS
            SSL_set_fd (ssl, fdCli);
            //printf("Client FD : %d\n", fdCli);
            int err = SSL_accept (ssl); //TLS Handshake
            CHK_SSL(err);
            //printf ("SSL connection established!\n");

            //SLLTestConnection(ssl, fdCli); //Send/Recieve packet
            
            //Verify username, password
            //printf("Authenticaing User\n");
            if(AuthenticateUser(ssl)) {
                printf("User Authentication Failed\n");
                close(fdCli);
                close(client[cid].pipefd[PIPE_FD_RD]);
                return 0;
            }
            //printf("User Authentication Successful\n");
            
            // Send New IP for tun interface to client
            assignClientIp(ssl, client, cid);

            while(1) {
                fd_set readFDSet;

                FD_ZERO(&readFDSet);
                FD_SET(fdCli, &readFDSet);
                FD_SET(client[cid].pipefd[PIPE_FD_RD], &readFDSet);
                
                //Wait untill data is availabe on an interface
                select(FD_SETSIZE, &readFDSet, NULL, NULL, NULL);

                // Process data from tun interface when available
                if (FD_ISSET(client[cid].pipefd[PIPE_FD_RD],  &readFDSet))
                    if(PipeToSocket(client[cid].pipefd[PIPE_FD_RD], ssl))
                        break;
                    
                // Process data from socket when available
                if (FD_ISSET(fdCli, &readFDSet)) 
                    if(socketToTun(ssl, tunfd))
                        break;
            } // while(1) client
            
            close(client[cid].pipefd[PIPE_FD_RD]); //Close read end of pipe
            
            //If server terminated process
            int st = SSL_get_shutdown(ssl);
            if(SSL_RECEIVED_SHUTDOWN != st) {
                SSL_shutdown(ssl);
            }
            SSL_free(ssl);
            close(fdCli);
            close(tunfd);
            return 0;
        } else { // The parent process : Server
            close(fdCli);
            close(client[cid].pipefd[PIPE_FD_RD]); //Close read end of pipe
        }
    } // while(1) server
    
    printf("Closing Server Connection\n");
    
    //todo how to close pipe for terminated client connection on server?
    
    // Close all active client connections (if open)
    for (int i = CLIENT_HOSTID_START; i < iClientHostId; i++) {
        close(client[i].pipefd[PIPE_FD_WR]); //Close write end of pipe
    }
        
    close(fdSer);   
    close(tunfd);
}
