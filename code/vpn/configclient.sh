#!/bin/bash

if [ "$#" -ne 1 ]; then
    TUN_IP=192.168.53.5
else
    TUN_IP=$1
fi

# Assigns an IP address to the tun0 interface and then activate it
# echo '# Assign IP to tun0 interface and activate it'
echo 'sudo ifconfig tun0' $TUN_IP '/24 up'
sudo ifconfig tun0 $TUN_IP/24 up

# setup routing at client (automatically done by above command)
# sudo route add -net 192.168.53.0/24 dev tun0

#------------------------------------------------------------------------

# direct packet for private network to tun0, to send to VPN server
# echo '# Direct packet for private interface to tun0'
echo 'sudo route add -net 192.168.60.0/24 dev tun0'
sudo route add -net 192.168.60.0/24 dev tun0

#------------------------------------------------------------------------
# Setup Firewall to block website via real interface
# sudo iptables -t mangle -A POSTROUTING -d 128.230.18.198 -o enp0s3 -j DROP
# sudo iptables -t mangle -A POSTROUTING -d 10.0.2.8 -o enp0s3 -j DROP

#------------------------------------------------------------------------
# Route traffic via tun
# sudo route add -net 128.230.18.198 netmask 255.255.255.255 dev tun0
# sudo route add -net 10.0.2.8 netmask 255.255.255.255 dev tun0
