#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>         //atoi
#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <sys/ioctl.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <netdb.h>
#include <termios.h>        //tcsetattr


#define BUFF_SIZE 2000

// TUN
#define TUN_DEV_NAME "/dev/net/tun"

// TCP
#define TCP_PORT_DEFAULT 4433

// SSL
#define HOSTNAME_DEFAULT "gupta.com"
#define CHK_SSL(err) if ((err) < 1) { ERR_print_errors_fp(stderr); exit(2); } 

// VPN Header
enum vpn_type {VPN_DATA, VPN_CONTROL};
enum vpn_control_flag {VPN_SUCCESS, VPN_FAIL, VPN_EXIT};

struct VpnHeader {
    enum vpn_type type;
    enum vpn_control_flag flag;
    int length;
};

// ---------------------- TUN --------------------------------

int createTunDevice() {
   int tunfd;
   struct ifreq ifr;
   memset(&ifr, 0, sizeof(ifr));

   ifr.ifr_flags = IFF_TUN | IFF_NO_PI;  

   tunfd = open(TUN_DEV_NAME, O_RDWR);
   ioctl(tunfd, TUNSETIFF, &ifr);       

   return tunfd;
}

int configureInterface(char *ip)
{
    if(fork() == 0) //Child Process
    {
        char *newargv[] = { "./configclient.sh", ip, NULL };
        char *newenviron[] = { NULL };
        
        execve(newargv[0], newargv, newenviron);
        
        perror("execve");   /* execve() returns only on error */
        exit(EXIT_FAILURE);
    }
}


// ---------------------- TLS --------------------------------

int verify_callback(int preverify_ok, X509_STORE_CTX *x509_ctx)
{
    char  buf[300];

    X509* cert = X509_STORE_CTX_get_current_cert(x509_ctx);
    X509_NAME_oneline(X509_get_subject_name(cert), buf, 300);
    printf("subject= %s\n", buf);

    if (preverify_ok == 1) {
        printf("Verification passed.\n");
    } else {
        int err = X509_STORE_CTX_get_error(x509_ctx);
        printf("Verification failed: %s.\n", X509_verify_cert_error_string(err));
    }
}

SSL* setupTLSClient(const char* hostname)
{
    SSL_METHOD *meth;
    SSL_CTX* ctx;
    SSL* ssl;

    // Step 0: OpenSSL library initialization 
    // This step is no longer needed as of version 1.1.0.
    SSL_library_init();
    SSL_load_error_strings();
    SSLeay_add_ssl_algorithms();
    
    // Step 1: SSL context initialization
    meth = (SSL_METHOD *)TLSv1_2_method();
    ctx = SSL_CTX_new(meth);

    // Step 2: Set up the certificate verification
    SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, NULL);
    if (SSL_CTX_load_verify_locations(ctx, NULL, "./ca_client") < 1)	//Certificate Location
    {	
	    printf("Error setting the verify locations. \n");
        exit(0);
    }
    
    // Step 3: Create a new SSL structure for a connection
    ssl = SSL_new (ctx);

    // Verify Hostname = Common Name in Certificate
    X509_VERIFY_PARAM *vpm = SSL_get0_param(ssl); 
    X509_VERIFY_PARAM_set1_host(vpm, hostname, 0);

    return ssl;
}

// ---------------------- TCP --------------------------------

struct in_addr getaddrinfowrapper(const char *hostname) 
{
    struct in_addr s_addr;
    struct addrinfo hints, *result;
    
    memset(&s_addr, 0, sizeof (struct in_addr));
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET; // AF_INET means IPv4 only addresses
    
    int error = getaddrinfo(hostname, NULL, &hints, &result);
    if (error) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
        return s_addr;
    }
    
    // The result may contain a list of IP address; we take the first one.
    struct sockaddr_in* ip = (struct sockaddr_in *) result->ai_addr;
    memcpy(&s_addr, &ip->sin_addr, sizeof (struct in_addr));
    //printf("IP Address: %s\n", (char *)inet_ntoa(s_addr));
    
    freeaddrinfo(result);
    return s_addr;
}

int setupTCPClient(const char* hostname, int port)
{
    struct sockaddr_in server_addr;
    
    // Create a TCP socket
    int sockfd= socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    // Fill in the destination information (IP, port #, and family)
    memset (&server_addr, '\0', sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr = getaddrinfowrapper(hostname);        // Get the IP address from hostname
    // server_addr.sin_addr.s_addr = inet_addr ("10.0.2.14"); 
    server_addr.sin_port   = htons (port);                      // PORT

    printf("Connecting to VPN Server at %s:%hu\n", inet_ntoa(server_addr.sin_addr), ntohs(server_addr.sin_port));
    
    // Connect to the destination
    connect(sockfd, (struct sockaddr*) &server_addr, sizeof(server_addr));

    return sockfd;
}

void SSLTestConnection(SSL *ssl, char* hostname)
{
    char buf[512];
    int len;
    
    printf("\nTesting Connection Start\n");
    //Send
    sprintf(buf, "GET / HTTP/1.1\nHost: %s\n", hostname);
    len = SSL_write(ssl, buf, strlen(buf));
    printf("Sent(%d):%s\n",len, buf);

    //Recieve
    len = SSL_read (ssl, buf, sizeof(buf) - 1);
    buf[len] = '\0';
    printf("Recieved(%d):%s\n",len, buf);
    printf("\nTesting Connection End\n");
}

int getClientIp(SSL *ssl)
{
    char ipNew[INET_ADDRSTRLEN];
    
    int len = SSL_read (ssl, ipNew, sizeof(ipNew));
    ipNew[len] = '\0';
    //printf("Recieved(%d):%s\n",len, ipNew);
    
    printf("Allocated IP for client : %s\n", ipNew);
    
    configureInterface(ipNew);
    
    return (len > 0 ? 0 : -1);
}

// ---------------------- Tunnel --------------------------------

int SSLtunSelected(int tunfd, SSL *ssl)
{
    static int cnt;
    int  len;
    char buff[BUFF_SIZE];

    // Read data from TUN Interface
    bzero(buff, BUFF_SIZE);
    len = read(tunfd, buff, BUFF_SIZE);
    
    //printf("%3d TUN(%d) : %c\n", cnt++, len, buff[0]);
 
    struct VpnHeader hdr;
    memset(&hdr, 0, sizeof hdr);
    hdr.type = VPN_DATA;
    hdr.length = len;
    
    //Write header to socket
    SSL_write(ssl, &hdr, sizeof (hdr));
    //printf("VPN Header Type(%d) Length(%d)\n", hdr.type, hdr.length);
    
    // Write data to socket 
    SSL_write(ssl, buff, len);
    return 0;
}

int SSLsocketSelected(int tunfd, SSL *ssl)
{
    static int cnt;
    int  len;
    char buff[BUFF_SIZE];

    bzero(buff, BUFF_SIZE);

    // Read data from socket
    len = SSL_read(ssl, buff, BUFF_SIZE - 1);
    if(len == 0) {
        int ret = SSL_get_error(ssl, len);
        printf("SSL_read Error : %d\n", ret);
        return 1;
    }
   
    //printf("%3d SOC(%d) : %c\n", cnt++, len, buff[0]);
 
    // Write to TUN Interface
    write(tunfd, buff, len);
    return 0;
}

int stopClient(SSL *ssl)
{
    char buff[64];
    char exitStr[] = "exit";
    
    fgets(buff, sizeof buff, stdin);
	if(strncmp(exitStr, buff, strlen(exitStr)) != 0)
	    return -1;
	
	//Send stop signal to server
    struct VpnHeader hdr;
    memset(&hdr, 0, sizeof hdr);
    hdr.type = VPN_CONTROL;
    hdr.flag = VPN_EXIT;
    
    //printf("VPN Header Type(%d) Length(%d)\n", hdr.type, hdr.length);
        
    //Write header to socket
    SSL_write(ssl, &hdr, sizeof hdr);
	return 0; 
}

// ---------------------------- login ----------------------------

char* getPassword()
{
    struct termios oflags, nflags;
    char *password = NULL;
    size_t plen = 0;
    int fd = fileno(stdin);
    //char *stream = "/dev/tty";
    //int fd = open(stream, O_RDWR);
    
    /* Get attributes */
    if (tcgetattr(fd, &oflags) != 0) {
        //close(fd);
        perror("tcgetattr");
        return NULL;
    }
    
    /* disabling echo */
    nflags = oflags;
    nflags.c_lflag &= ~ECHO;        // disable echo
    nflags.c_lflag |= ECHONL;       // echo new line

    if (tcsetattr(fd, TCSANOW, &nflags) != 0) {
        //close(fd);
        perror("tcsetattr");
        return NULL;
    }

    // Get Password
    int len = getline(&password, &plen, stdin);
    password[len - 1] = '\0';
    
    /* restore terminal */
    if (tcsetattr(fd, TCSANOW, &oflags) != 0) {
        free (password);
        //close(fd);
        perror("tcsetattr");
        return NULL;
    }

    //close(fd);
    return password;
}

int AuthenticateUser(SSL *ssl)
{
    char username[64], *password;
    
    printf("Enter Username : ");
    scanf("%s", username);
    
    //todo fix the getPassword() function since getpass is obsolete
    //printf("Enter Password : ");
    //password = getPassword();
    password =  getpass("Enter Password : ");       
    if(NULL == password) {
        printf("getPassword Failed\n");    
        return -1;
    }
    //printf("Entered Password: %s\n", password);
    
    int ulen = strlen(username);
    username[ulen++] = '\\';
    strcpy(username + ulen, password);
    //free(password);

    // Send username/password to server
    int len = SSL_write(ssl, username, strlen(username));
    //printf("Sending (%d) : %s\n", len, username);

    //Get status from server
    struct VpnHeader hdr;
    memset(&hdr, 0, sizeof hdr);
    len = SSL_read (ssl ,&hdr, sizeof(hdr));
    //printf("Recieved (%d): Type(%d) Flag(%d)\n", len, hdr.type, hdr.flag);

    return hdr.flag == VPN_SUCCESS ? 0 : -1;
}

// ---------------------- MAIN --------------------------------

int main (int argc, char * argv[])
{
    char *hostname = HOSTNAME_DEFAULT;  //TCP
    int port = TCP_PORT_DEFAULT;

    // Accept commandline args
    if (argc > 1) hostname = argv[1];
    if (argc > 2) port = atoi(argv[2]);

    //printf("Connecting to Server at %s:%d\n", hostname, port);
       
    
    int fdSer = setupTCPClient(hostname, port); // Create a TCP connection with server
    
    SSL *ssl = setupTLSClient(hostname);    // TLS initialization
    SSL_set_fd(ssl, fdSer);
    int err = SSL_connect(ssl);             //TLS handshake
    CHK_SSL(err);
    //printf("SSL connection is successful\n");
    //printf("SSL connection using %s\n", SSL_get_cipher(ssl));
	
    //SSLTestConnection(ssl, hostname); //Send/Recieve packet
    
    //Login Prompt to authenticate user cred
    if(AuthenticateUser(ssl)) {
        printf("Authentication Failed. Invalid username/password\n");
        printf("Closing Client Connection\n");
        SSL_shutdown(ssl);
        SSL_free(ssl);
        close(fdSer);        
        return 0;
    }
    //printf("Authentication Successful.\n");

    int tunfd = createTunDevice();      // Setup TUN Interface
    getClientIp(ssl);                   //Get IP for tun interface from server

    while (1) {
        fd_set readFDSet;

        FD_ZERO(&readFDSet);
        FD_SET(fdSer, &readFDSet);
        FD_SET(tunfd, &readFDSet);
        FD_SET(fileno(stdin), &readFDSet);
    
        //Wait untill data is availabe on an interface        
        select(FD_SETSIZE, &readFDSet, NULL, NULL, NULL);
    
        // Process data from TUN Interface
        if (FD_ISSET(tunfd,  &readFDSet))
            if(SSLtunSelected(tunfd, ssl))
                break;
           
        // Process data from socket
        if (FD_ISSET(fdSer, &readFDSet))
            if(SSLsocketSelected(tunfd, ssl))
                break;
                
        if (FD_ISSET(fileno(stdin), &readFDSet)) {
            if(stopClient(ssl) == 0)   // Enter "exit" to stop
                break; 
	    }
    }
    
    printf("Closing Client Connection\n");
    int ret = SSL_shutdown(ssl);
    if(ret != 1) {
        //printf("Waiting for server termination ack: (%d)\n", ret);
        ret = SSL_shutdown(ssl);
        //printf("Server termination ack received: (%d)\n", ret);
    }
    SSL_free(ssl);
    close(fdSer);   
    close(tunfd);
}

