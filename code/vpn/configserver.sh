#!/bin/bash

# Enable IP forwarding for server to behave like a gateway
# echo '# Enable IP forwarding'
echo 'sudo sysctl net.ipv4.ip_forward=1'
sudo sysctl net.ipv4.ip_forward=1

#------------------------------------------------------------------------

# Assigns an IP address to the tun0 interface and then activate it
# echo '# Assign IP to tun0 interface and activate it'
echo 'sudo ifconfig tun0 192.168.53.1/24 up'
sudo ifconfig tun0 192.168.53.1/24 up

# setup routing at server (automatically done by above command)
# sudo route add -net 192.168.53.0/24 dev tun0

#------------------------------------------------------------------------

# To Access Internet
# NAT Limitation on Virtual Machine
# Clean all Iptable rules:
# sudo iptables -F
# sudo iptables -t nat -F

# Add a rule on postrouting position to the NatNetwork adapter (enp0s3) connected to VPN server.
# sudo iptables -t nat -A POSTROUTING -j MASQUERADE -o enp0s3
